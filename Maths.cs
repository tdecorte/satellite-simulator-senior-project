﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    public static class Maths
    {
        
        /// <summary>
        /// Converts degrees to corresponding radian values
        /// </summary>
                
        public static double toRadians(double decimalVal)
        {
            return (Math.PI / 180) * decimalVal;
        }


        /// <summary>
        /// Converts radian values to corresponding degrees
        /// </summary>

        public static double toDegrees(double radianVal)
        {
            return radianVal * 180 / Math.PI;
        }
    }
}
