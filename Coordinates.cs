﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Drawing.Drawing2D;


namespace SatelliteSimulator
{
    class Coordinates
    {
        // Coordinate variables:

        private double m_prevLatRad;

        private double m_latitude;
        private double m_longitude;


        private double m_x;
        private double m_y;
        private double m_z;

        private double m_rotatedX;
        private double m_rotatedY;
        private double m_rotatedZ;

        private double m_degreesToLat;


        // Accessors

        public double latitude { get { return m_latitude; } set { m_latitude = value; } }
        public double longitude { get { return m_longitude; } set { m_longitude = value; } }
             

        

    }
}