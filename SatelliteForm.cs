using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using GEPlugin;

namespace SatelliteSimulator
{
    [ComVisibleAttribute(true)]
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public partial class SatelliteForm : Form
    {

        private const string PLUGIN_URL =
           @"C:\seniorProject\senior-project-repo\pluginhost.html";

        private bool m_allSelected = false;
        private bool m_changeInterval = false;
        private bool m_forecast = false;
        private bool m_firstLoop = true;
        private bool m_initialRun = true;
        private bool m_single = false;
        private bool m_singleSelected = false; 
        private int m_loopCount = 0;
        //private int m_tickCount = 0;
        private int m_timeLeft;
    
        private DateTime m_dateTime;
        private DataMapper m_dm;
        private IGEPlugin m_ge = null;
        private IKmlPlacemark m_satellites = null;        
        private List<int> m_selectedSats;
        private Timer m_timer;
        private Time m_time;
        private Time m_satTime;
                
        


        public SatelliteForm()
        {
            m_satTime = new Time();
            InitializeComponent();

            m_timer = new Timer();
            m_timer.Interval = 25000;
            m_timer.Tick += Timer_Tick;

                       
            m_initialRun = false;           
            m_dateTime = DateTime.Now;
            
            
            m_timer.Interval = 6000; // update each minute. 60 ms = 1 min


            m_timer.Start();

            m_selectedSats = new List<int>();
            m_firstLoop = true;
        }


        private void Timer_Tick(Object sender, EventArgs e)
        {
            m_timeLeft = 60;

            if (m_forecast == false)
                m_time = new Time();

            //var diff = DateTime.Now;
            //this.textBox1.Text = diff.ToString();

            if (m_allSelected == true)
            {

                foreach (int idx in m_dm.satNums)
                {
                    m_loopCount++;

                    if (m_loopCount > m_dm.satNums.Count)
                        m_firstLoop = false;

                    displaySatPath(populateSatellite(idx), idx, m_firstLoop);

                }

                m_changeInterval = true;
            }

            if (m_singleSelected == true)
            {
                foreach (int idx in m_selectedSats)
                {
                    displaySatPath(populateSatellite(idx), idx, m_firstLoop);
                }

                m_changeInterval = true;
            }
                        

            if (m_changeInterval == true)
                m_timer.Interval = 60000; // update each minute. 60 ms = 1 min

            
            
        }




        private void Form1_Load(object sender, EventArgs e)
        {
            webBrowserCtrl.Navigate(PLUGIN_URL);
            webBrowserCtrl.ObjectForScripting = this;

            m_dm = new DataMapper();
            //m_dm.populateListBox(this.listBox1);
            m_dm.populateCheckedListBox(this.checkedListBox1);
        }


        // called from initCallback in JavaScript

        public void JSInitSuccessCallback_(object pluginInstance)
        {
            m_ge = (IGEPlugin)pluginInstance;
        }


        // called from failureCallback in JavaScript

        public void JSInitFailureCallback_(string error)
        {
            MessageBox.Show("Error: " + error, "Plugin Load Error", MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
        }



        private void webBrowserCtrl_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }



        private Satellite populateSatellite(int satelliteNum)
        {

            DataMapper dm = new DataMapper();

            dm.getData(satelliteNum);
            Satellite satellite = new Satellite();

            satellite.name = dm.satelliteNum.ToString();
            satellite.almanacData.eccentricity = dm.eccentricity;
            satellite.almanacData.timeOfApplicability = dm.timeOfApplicability;
            satellite.almanacData.inclination = dm.inclination;
            satellite.almanacData.rateOfRA = dm.rateOfRA;
            satellite.almanacData.sqrtA = dm.sqrtA;
            satellite.almanacData.rightAscension = dm.rightAscenAtWeek;
            satellite.almanacData.argumentOfPerigee = dm.argumentPerigee;
            satellite.almanacData.meanAnomaly = dm.meanAnomaly;
            satellite.almanacData.af0 = dm.af0;
            satellite.almanacData.af1 = dm.af1;
            satellite.almanacData.week = dm.week;

            return satellite;
        }


                
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = (sender as ListBox).SelectedItem.ToString();

            // Parse integer value from string
            char delimeter = ' ';
            string[] splitStr = selected.Split(delimeter);

            int select = Convert.ToInt32(splitStr[1]);
            m_single = true;

            m_selectedSats.Add(select);
            displaySatPath(populateSatellite(select), select, false);

            m_firstLoop = false; 

            if (m_singleSelected == false)
                m_singleSelected = true;

            else
                m_singleSelected = false;

        }



        // Iterates through specified number of seconds, calling functions to recalculate
        // satellites position at each new second.
        // Sets text boxes and draws new icon at each calculated lat/long/alt.

        private void displaySatPath(Satellite satellite, int satNum, bool firstLoop)
        {

            m_ge.getTime().getControl().setVisibility(m_ge.VISIBILITY_SHOW);

            



            double secsFromEphemeris = m_satTime.secsFromEphemeris(satellite.almanacData.timeOfApplicability, satellite.almanacData.week);
            double ephemerisSeconds = m_satTime.ephemerisSeconds(satellite.almanacData.timeOfApplicability, satellite.almanacData.week);
            //textBox3.Text = ephemerisSeconds.ToString();


            KeplerEquations keplerEquations = new KeplerEquations(ephemerisSeconds,
                                                                  satellite.almanacData.eccentricity,
                                                                  satellite.almanacData.semiMajorAxis,
                                                                  satellite.almanacData.meanAnomaly);





            // Get time from index value

            int idx = (int)m_satTime.timeFromEphemeris(satellite.almanacData.timeOfApplicability, satellite.almanacData.week);
            //textBox2.Text = idx.ToString();


            // Calculate new coordinates

            CoordinateCalculations calcs = new CoordinateCalculations(ref satellite, ref keplerEquations, idx, m_satTime, secsFromEphemeris);
            




            if (m_firstLoop == true)
            {
                string satelliteId = "satellite" + satNum.ToString();
                m_satellites = m_ge.createPlacemark(satelliteId);
                var timeSpans = m_ge.createTimeSpan("");
            }

            else if (m_firstLoop == false && m_single == false)
            {
                    m_satellites = (IKmlPlacemark)m_ge.getElementById("satellite" + satNum);
                    m_satellites.setVisibility(0);
                    m_satellites.setVisibility(1);
            }


            m_satellites.setName("");

            var icons = m_ge.createIcon("");


            icons.setHref("https://889c172c-a-62cb3a1a-s-sites.googlegroups.com/" +
                        "site/satellitesimulation/kml-files/integralTransparent.png?" +
                        "attachauth=ANoY7crC2Edb1XdFTOAHniOiDGin1-t-Rg00j3kA4fhgI_iNGb2oy-" +
                        "Ow99ZnRcvQhsa488OtwJ2W4LTh8HaeLUgdrgf2vmJcMPpAXCre0K0BEldEq8naqOl" +
                        "ns1foNwyHeyhg6QIAYYVc7jpwXm0wP0DMZitQQTclt5-UPOt5roaPu_VqF84VOMbj" +
                        "W7IH4jdFOOpu1NrKwvhN0JuAN7n0yobhpvuy6XoSCsEuMb--gn9bCAJ-8Cjh0GYPs" +
                        "OodQ_pP9IEXpjkJuOPn&attredirects=0");


            var styles = m_ge.createStyle("");
            styles.getIconStyle().setIcon(icons);
            styles.getIconStyle().setScale(25);
            m_satellites.setStyleSelector(styles);



            // Ensure positions withing scope

            satellite.coordinates.latitude %= 180;
            satellite.coordinates.longitude %= 360;




            // Create point and set data for location

            var points = m_ge.createPoint("");

            points.setLatitude(satellite.coordinates.latitude);
            points.setLongitude(satellite.coordinates.longitude);
            points.setAltitudeMode(m_ge.ALTITUDE_RELATIVE_TO_GROUND);
            points.setAltitude(satellite.altitude);
            //points.setAltitude(70000);



            m_satellites.setGeometry(points);




            // Add information to placemark balloon

            m_satellites.setDescription(
                satellite.name +
                "\nEccentricity: " + satellite.almanacData.eccentricity +
                "\nOrbital Altitude: " + satellite.altitude +
                "\nVelocity: " + satellite.velocity +
                "\nOrbital Period: " + (keplerEquations.orbitalPeriod / 60) +
                "\nInclination: " + Maths.toDegrees(satellite.almanacData.inclination) +
                "\nOrbit Type: " + satellite.orbitalType +
                "\nLatitude: " + satellite.coordinates.latitude +
                "\nLongitude: " + satellite.coordinates.longitude);



            // Draw placemark on map

            m_ge.getFeatures().appendChild(m_satellites);


            
                var lookat = m_ge.getView().copyAsLookAt(m_ge.ALTITUDE_RELATIVE_TO_GROUND);

                lookat.setLatitude(satellite.coordinates.latitude);    
                lookat.setLongitude(satellite.coordinates.longitude);
             if (m_allSelected == false)
            {              
                m_ge.getView().setAbstractView(lookat);
            }
        }





        private void showSun_Click(object sender, EventArgs e)
        {
            m_ge.getSun().setVisibility(1);
        }

        private void hideSun_Click(object sender, EventArgs e)
        {
            m_ge.getSun().setVisibility(0);
        }

        private void skyViewButton_Click(object sender, EventArgs e)
        {
            m_firstLoop = true;
            m_ge.getOptions().setMapType(m_ge.MAP_TYPE_SKY);
        }

        private void earthViewButton_Click(object sender, EventArgs e)
        {
            m_ge.getOptions().setMapType(m_ge.MAP_TYPE_EARTH);
        }

        private void viewAllButton_Click(object sender, EventArgs e)
        {
            //m_timeLeft = m_dateTime.Second;
            timer1.Enabled = true;

            PleaseWaitForm pleaseWait = new PleaseWaitForm();
            pleaseWait.Show();

            clearButton_Click(sender, e);            

            if (m_allSelected == false)
                m_allSelected = true;

            else
                m_allSelected = false;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            webBrowserCtrl.Navigate(PLUGIN_URL);
            webBrowserCtrl.ObjectForScripting = this;

            m_allSelected = false;
            m_changeInterval = false;
            m_firstLoop = true;
            m_forecast = false; 
            m_loopCount = 0;
            m_selectedSats.Clear();
            m_single = false;
            m_timer.Interval = 6000;
            m_timer.Tick += Timer_Tick;
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            DateTime dateTime = ((DateTimePicker)sender).Value;

            //Time time = new Time();
            m_satTime.forecastedTime(dateTime);

            m_forecast = true;
            //time.forecastedTime(dateTime);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (m_timeLeft > 0)
            {
                m_timeLeft -= 1;
                timeLabel.Text = "Time until update: " + m_timeLeft.ToString() + " seconds.";
            }

            else
            {
                m_timeLeft = 59;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = (sender as CheckedListBox).SelectedItem.ToString();

            

            // Parse integer value from string
            char delimeter = ' ';
            string[] splitStr = selected.Split(delimeter);

            int select = Convert.ToInt32(splitStr[1]);
            m_single = true;

            for (int idx = 0; idx < checkedListBox1.Items.Count; ++idx)
            {
                if (select != (idx+1))
                    checkedListBox1.SetItemChecked(idx, false);

                else
                    checkedListBox1.SetItemChecked(idx, true);
            }



            m_selectedSats.Add(select);
            displaySatPath(populateSatellite(select), select, false);

            m_firstLoop = false;

            if (m_singleSelected == false)
                m_singleSelected = true;

            else
                m_singleSelected = false;
        }



    }
}