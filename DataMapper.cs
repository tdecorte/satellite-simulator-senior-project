﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;



namespace SatelliteSimulator
{
    class DataMapper
    {
        private int m_id;
        private int m_satelliteNum;
        private double m_health;
        private double m_eccentricity;
        private double m_timeOfApplicability;
        private double m_inclination;
        private double m_rateOfRA;
        private double m_sqrtA;
        private double m_rightAscensionWeek;
        private double m_argumentPerigee;
        private double m_meanAnomaly;
        private double m_af0;
        private double m_af1;
        private double m_week;
        private List<int> m_satNums;

        public int id { get { return m_id; } }
        public int satelliteNum { get { return m_satelliteNum; } }
        public double health { get { return m_health; } }
        public double eccentricity { get { return m_eccentricity; } }
        public double timeOfApplicability { get { return m_timeOfApplicability; } }
        public double inclination { get { return m_inclination; } }
        public double rateOfRA { get { return m_rateOfRA; } }
        public double sqrtA { get { return m_sqrtA; } }
        public double rightAscenAtWeek { get { return m_rightAscensionWeek; } }
        public double argumentPerigee { get { return m_argumentPerigee; } }
        public double meanAnomaly { get { return m_meanAnomaly; } }
        public double af0 { get { return m_af0; } }
        public double af1 { get { return m_af1; } }
        public double week { get { return m_week; } }
        public List<int> satNums { get { return m_satNums; } }


        /*
        public void populateListBox(ListBox listbox)
        {
            

            // need to move to xml file containing information

            string connectionString = ConnectionString.connection();

            m_satNums = new List<int>();
            MySqlConnection connection = null;

            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();

                string sqlStmt = "SELECT * from satellites";

                MySqlDataAdapter adapter = new MySqlDataAdapter(sqlStmt, connection);

                DataSet dataset = new DataSet();

                adapter.Fill(dataset, "satellites");
                DataTable table = dataset.Tables["satellites"];

                
                foreach (DataRow row in dataset.Tables["satellites"].Rows)
                {
                    Button testButton = new Button();
                    RadioButton rb = new RadioButton();
                    listbox.Items.Add(rb);
                    //listbox.Items.Add(("PRN: " + row["satelliteNum"]));
                    satNums.Add(Convert.ToInt32(row["satelliteNum"].ToString()));                    
                }

            }
            catch (MySqlException exception)
            {
                switch (exception.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server");
                        break;

                    case 1042:
                        //MessageBox.Show("Unable to connect to any of the specified MySQL hosts");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password");
                        break;
                }
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

        }
        */ 

        public void getData(int satelliteNum)
        {
            Satellite sat = new Satellite();

            string connectionString = ConnectionString.connection();
            

            MySqlConnection connection = null;

            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();

                string sqlStmt = "SELECT * from satellites WHERE satellites.satelliteNum = " + satelliteNum;

                MySqlCommand cmd = new MySqlCommand(sqlStmt, connection);
                MySqlDataReader reader = cmd.ExecuteReader();


                if (reader.Read() == true)
                {
                    m_id = reader.GetInt32("id");
                    m_satelliteNum = reader.GetInt32("satelliteNum");
                    m_health = reader.GetDouble("health");
                    m_eccentricity = reader.GetDouble("Eccentricity");
                    m_timeOfApplicability = reader.GetDouble("timeOfApplicability");
                    m_inclination = reader.GetDouble("inclination");
                    m_rateOfRA = reader.GetDouble("rateOfRA");
                    m_sqrtA = reader.GetDouble("sqrtA");
                    m_rightAscensionWeek = reader.GetDouble("rightAscensionWeek");
                    m_argumentPerigee = reader.GetDouble("ArgumentPerigee");
                    m_meanAnomaly = reader.GetDouble("meanAnomaly");
                    m_af0 = reader.GetDouble("af0");
                    m_af1 = reader.GetDouble("af1");
                    m_week = reader.GetDouble("week");
                }


            }
            catch (MySqlException exception)
            {
                switch (exception.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server");
                        break;

                    case 1042:
                        //MessageBox.Show("Unable to connect to any of the specified MySQL hosts");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password");
                        break;
                }
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
          
            
        }


        
        public void populateCheckedListBox(CheckedListBox listbox)
        {
            // need to move to xml file containing information

            string connectionString = ConnectionString.connection();

            m_satNums = new List<int>();
            MySqlConnection connection = null;

            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();

                string sqlStmt = "SELECT * from satellites";

                MySqlDataAdapter adapter = new MySqlDataAdapter(sqlStmt, connection);

                DataSet dataset = new DataSet();

                adapter.Fill(dataset, "satellites");
                DataTable table = dataset.Tables["satellites"];

                
                foreach (DataRow row in dataset.Tables["satellites"].Rows)
                {
                    //Button testButton = new Button();
                    //RadioButton rb = new RadioButton();
                    //listbox.Items.Add(rb);
                    listbox.Items.Add(("PRN: " + row["satelliteNum"]));
                    satNums.Add(Convert.ToInt32(row["satelliteNum"].ToString()));
                }

                listbox.CheckOnClick = true;

            }
            catch (MySqlException exception)
            {
                switch (exception.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server");
                        break;

                    case 1042:
                        //MessageBox.Show("Unable to connect to any of the specified MySQL hosts");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password");
                        break;
                }
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
           
        }
         
    }
}
