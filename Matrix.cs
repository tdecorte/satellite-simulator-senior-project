﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class Matrix
    {
        private double[,] row;

        public Matrix() { }

        public Matrix(double val1, double val2, double val3,
            double val4, double val5, double val6,
            double val7, double val8, double val9)
        {
            row = new double[3, 3];
            row[0, 0] = val1;
            row[0, 1] = val2;
            row[0, 2] = val3;
            row[1, 0] = val4;
            row[1, 1] = val5;
            row[1, 2] = val6;
            row[2, 0] = val7;
            row[2, 1] = val8;
            row[2, 2] = val9;
        }


        public Matrix(double val1, double val2, double val3)
        {
            row = new double[3, 1];
            row[0, 0] = val1;
            row[1, 0] = val2;
            row[2, 0] = val3;
        }


        public Matrix(double[,] vector)
        {
            int size = vector.Length / 3;
            int count = 0;
            row = new double[3, size];

            if (size == 1)
            {
                foreach (double element in vector)
                {
                    row[count, count / 3] = element;
                    count++;
                }
            }

            else
                foreach (double element in vector)
                {
                    row[count / 3, count % 3] = element;
                    count++;
                }
        }


        public double[,] getRow { get { return row; } }


        // Multiplies matrix 1 by matrix 2 (order matters)

        public static Matrix multiply(Matrix matrix, Matrix matrix2)
        {
            double columnTotal = 0;
            int size = 3;

            if (matrix2.row.Length == 3)
                size = 1;

            double[,] vector = new double[3, size];
            int rowIdx = 0;
            int colIdx = 0;
            int multIdx = 0;
            int loopCount = 0;

            for (int i = 0; i < matrix.row.Length; i++)
            {
                if (i == 3 && matrix2.row.Length == 3)
                    break;

                for (int idx = 0; idx <= matrix2.row.Rank; idx++)
                {
                    loopCount++;

                    if (loopCount % 10 == 0)
                        multIdx++;

                    int rowIndex = i % 3;
                    columnTotal += matrix.row[rowIndex, idx] * matrix2.row[idx, multIdx];

                    if (idx == matrix2.row.Rank)
                    {
                        vector[rowIdx, colIdx] = columnTotal;
                        rowIdx++;
                        columnTotal = 0;

                        if (rowIdx == 3)
                        {
                            colIdx++;
                            rowIdx = 0;
                        }

                    }
                }
            }

            return new Matrix(vector);
        }


    }
}