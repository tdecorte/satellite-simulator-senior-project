﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class AlmanacData
    {
        private double m_eccentricity;
        private double m_epochTime;
        private double m_inclination;
        private double m_rateOfRightAscension;
        private double m_sqrtA;
        private double m_rightAscension;
        private double m_argumentOfPerigee;
        private double m_meanAnomaly;
        private double m_timeOfApplicability;
        private double m_week;

        private double m_af0; // Zero-order clock correction;
        private double m_af1 = .3638; // 1-order clock correction;


        public double eccentricity { get { return m_eccentricity; } set { m_eccentricity = value; } }
        public double epochTime { get { return m_epochTime; } set { m_epochTime = value; }  }
        public double inclination { get { return m_inclination; } set { m_inclination = value; } }
        public double rateOfRA { get { return m_rateOfRightAscension; } set { m_rateOfRightAscension = value; } }
        public double semiMajorAxis { get { return m_sqrtA * m_sqrtA; } set { m_sqrtA = Math.Sqrt(value); } }
        public double rightAscension { get { return m_rightAscension; } set { m_rightAscension = value; } }
        public double argumentOfPerigee { get { return m_argumentOfPerigee; } set { m_argumentOfPerigee = value; } }
        public double meanAnomaly { get { return m_meanAnomaly; } set { m_meanAnomaly = value; } }
        public double af0 { get { return m_af0; } set { m_af0 = value; } }
        public double af1 { get { return m_af1; } set { m_af1 = value; } }
        public double sqrtA { get { return m_sqrtA; } set { m_sqrtA = value; } }
        public double timeOfApplicability { get { return m_timeOfApplicability; } set { m_timeOfApplicability = value; } }
        public double week { get { return m_week; } set { m_week = value; } }
    }
}
