﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class Time
    {
        private DateTime m_epochStart = new DateTime(1980, 01, 06, 0, 0, 1);

        private DateTime m_vernalEquinox1979 = new DateTime(1979, 03, 21, 05, 22, 03);

        private static DateTime m_dateTime;
        private static DateTime m_dateTimeEnd;

        private double m_secSiderealDay = 86164;
        private double m_secPerDay = 86400;
        private double m_degInCircle = 360.0;

        private double m_ephemerisSeconds;


        public static double increaseRate { get { return 1; } }
        public double ephemerisInSeconds { get { return m_ephemerisSeconds; } }
        public DateTime epochStart { get { return m_epochStart; } }


        public Time()
        {
            m_dateTime = DateTime.Now;
            m_dateTimeEnd = DateTime.Now.AddSeconds(1);

            var info = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time");

            DateTimeOffset localServerTime = DateTimeOffset.Now;

            DateTimeOffset usersTime = TimeZoneInfo.ConvertTime(localServerTime, info);

            DateTimeOffset utc = localServerTime.ToUniversalTime();

            m_dateTime = utc.DateTime;
        }

        
        public double timeFromVE1980()
        {
            long timeDiff = (m_dateTime.Ticks - m_epochStart.Ticks);
            TimeSpan elapsedSpan = new TimeSpan(timeDiff);

            long timeDiffSecs = (long)elapsedSpan.TotalSeconds;

            return timeDiffSecs;
        }


        public long timeFromEphemeris(double timeOfApplicability, double week)
        {
            DateTime ephemeris = ephemerisTime(timeOfApplicability, week);
            
            long timeDiff = (m_dateTime.Ticks - ephemeris.Ticks);
            TimeSpan elapsedSpan = new TimeSpan(timeDiff);

            long timeDiffSecs = (long)elapsedSpan.TotalSeconds;



            // test time from ra
            //int secsInWeek = 86400 * 7;
            //int secSinceRa = (742 * secsInWeek) + (int)timeOfApplicability;
            //DateTime raTime = epochStart.AddSeconds(secSinceRa);
            //timeDiffSecs = m_dateTime.Subtract(raTime).TotalSeconds;


            return timeDiffSecs;
        }


        public double alignOriginTime()
        {
            double seconds = (m_epochStart.AddSeconds(m_dateTime.Second)).Second % 86314;
            double result = seconds * (m_degInCircle / m_secSiderealDay);

            return result;
        }


        public int currentTime(double timeOfApplicability, double week)
        {
            double timeOfApp = (week * m_secPerDay) + timeOfApplicability;
            DateTime timeApp = m_epochStart.AddSeconds(timeOfApp);

            return m_dateTime.Subtract(timeApp).Seconds;
        }


        public DateTime ephemerisTime(double timeOfApplicability, double week)
        {
            int daysInWeek = 7;
            double timeOfApp = (week * daysInWeek * m_secPerDay) + timeOfApplicability;

            DateTime time = m_epochStart.AddSeconds(timeOfApp);

            return time;
        }


        public double ephemerisSeconds(double timeOfApplicability, double week)
        {
            int daysInWeek = 7;
            m_ephemerisSeconds = (week * daysInWeek * m_secPerDay) + timeOfApplicability;

            return m_ephemerisSeconds;
        }


        public void forecastedTime(DateTime time)
        {
            m_dateTime = time;
        }


        public long veSecsShift()
        {
            long timeDiff = (m_dateTime.Ticks - m_vernalEquinox1979.Ticks);
            TimeSpan elapsedSpan = new TimeSpan(timeDiff);

            long timeDiffSecs = (long)elapsedSpan.TotalSeconds;


            return timeDiffSecs;
        }


        public long secsFromEpoch()
        {
            long timeDiff = (m_dateTime.Ticks - m_epochStart.Ticks);
            TimeSpan elapsedSpan = new TimeSpan(timeDiff);

            long timeDiffSecs = (long)elapsedSpan.TotalSeconds;


            return timeDiffSecs;
        }


        public long secsVeToEphemeris()
        {
            long timeDiff = (m_epochStart.Ticks - m_vernalEquinox1979.Ticks);
            TimeSpan elapsedSpan = new TimeSpan(timeDiff);

            long timeDiffSecs = (long)elapsedSpan.TotalSeconds;


            return timeDiffSecs;
        }


        public double secsFromEphemeris(double timeOfApplicability, double week)
        {
            int daysInWeek = 7;
            double timeOfApp = (week * daysInWeek * m_secPerDay) + timeOfApplicability;
            return timeOfApp;
        }

    }
}
