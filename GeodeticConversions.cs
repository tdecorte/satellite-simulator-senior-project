﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class GeodeticConversions
    {

        private double m_latitude;
        private double m_longitude;
        private double m_altitude;

        private double m_x;
        private double m_y;
        private double m_z;


        public double latitude { get { return m_latitude; } }
        public double longitude { get { return m_longitude; } }
        public double altitude { get { return m_altitude; } }

        public double x { get { return m_x; } }
        public double y { get { return m_y; } }
        public double z { get { return m_z; } }

        


        
        /// <summary>
        /// Calculates normal radius
        /// </summary>

        private double calcR_n()
        {
            double f = WGS84.flattening;
            double eccentricitySquared = (2 * f) - (f * f);
            double radius = WGS84.equatorialRadius / (Math.Sqrt(1 - eccentricitySquared *
                        Math.Sin(Maths.toRadians(m_latitude)) * Math.Sin(Maths.toRadians(m_latitude))));

            return radius;
        }




        /// <summary>
        /// Converts geodetic LLA coordinates to geodetic cartesian coordinates
        /// </summary>

        public void geodeticLLA2cartesian(double latitude, double longitude, double altitude)
        {
            m_latitude = latitude;
            double radius = calcR_n();
            
            m_x = (radius + altitude) * Math.Cos(Maths.toRadians(latitude)) * Math.Cos(Maths.toRadians(longitude));
            m_y = (radius + altitude) * Math.Cos(Maths.toRadians(latitude)) * Math.Sin(Maths.toRadians(longitude));
            m_z = (radius * (1 - WGS84.eccentricitySquared) + altitude) * Math.Sin(Maths.toRadians(latitude));
        }




        /// <summary>
        /// Calculates converging latitude until desired precision has been achieved
        /// </summary>

        private void convergingLatitude(ref double reducedLat, ref double geodeticLat, double ePrimeSquare, double p)
        {
            double flattening = WGS84.flattening;

            reducedLat = Maths.toRadians(Math.Atan(((1 - flattening) * Math.Sin(geodeticLat)) / Math.Cos(geodeticLat)));

            ePrimeSquare = (WGS84.eccentricitySquared * (1 - flattening)) / (1 - WGS84.eccentricitySquared);

            double B = ((z + (ePrimeSquare * WGS84.equatorialRadius * Math.Pow(Math.Sin(reducedLat), 3))) /
                (p - (WGS84.eccentricitySquared * WGS84.equatorialRadius * Math.Pow(Math.Cos((reducedLat)), 3))));


            geodeticLat = (Math.Atan(B));
        }




        /// <summary>
        /// Bowring's method
        /// Converts catesian coordinates on x,y,z plane to geodetic latitude, longitude and altitude
        /// </summary>

        public void cartesian2geodeticLLA(double x, double y, double z)
        {
            double flattening = WGS84.flattening;
            double p = Math.Sqrt((x * x) + (y * y));

            
            double lat, N;
       
            double theta = Math.Atan2((z * WGS84.equatorialRadius) , (p * WGS84.semiMinorAxis));
          
            m_longitude = Maths.toDegrees(Math.Atan2(y, x));
           


            double e2 = Math.Sqrt((Math.Pow(WGS84.equatorialRadius, 2) - Math.Pow(WGS84.semiMinorAxis, 2)) / Math.Pow(WGS84.semiMinorAxis, 2));
            double e = Math.Sqrt((Math.Pow(WGS84.equatorialRadius, 2) - Math.Pow(WGS84.semiMinorAxis, 2)) / Math.Pow(WGS84.equatorialRadius, 2));
            lat = Math.Atan2(((z + Math.Pow(e2, 2) * WGS84.semiMinorAxis * Math.Pow(Math.Sin(theta), 3))) , 
                ((p - Math.Pow(e, 2) * WGS84.equatorialRadius * Math.Pow(Math.Cos(theta), 3))));

            m_latitude = Maths.toDegrees(lat);

            N = WGS84.equatorialRadius / (Math.Sqrt(1 - Math.Pow(e * Math.Sin(Maths.toRadians(m_latitude)), 2)));

            //m_altitude = ((p / Math.Cos(theta)) - N);         

        }




    }
}
