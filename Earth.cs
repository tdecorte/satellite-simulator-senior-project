﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    public static class Earth
    {
        private static readonly double m_earthMass = 5972198600000000000000000.0;
        private static readonly double m_avgEarthRadius = 6378100.0;
        private static readonly double m_gravitationalConstant = .00000000006673848;
        private static readonly double m_axialTilt = 23.439281;
        private static readonly double m_degInCircle = 360.0;


        public static double mass { get { return m_earthMass; } }
        public static double avgRadius { get { return m_avgEarthRadius; } }
        public static double radiusAtEquator { get { return WGS84.equatorialRadius; } }
        public static double gravitationalConst { get { return m_gravitationalConstant; } }
        public static double axialTilt { get { return m_axialTilt; } }

        public static double rotationRate { get { return m_degInCircle / sideRealSeconds(); } }



        /// <summary>
        /// Returns one side real day in hours
        /// </summary>
        
        public static double sideRealHours()
        {
            double hours = 23.0;
            double minutes = 56.0;
            double seconds = 4.0916;

            double mins2hours = minutes / 60;
            double seconds2hours = seconds / 3600;


            return (hours + mins2hours + seconds2hours);
        }



        /// <summary>
        /// Returns one side real day in seconds
        /// </summary>

        public static double sideRealSeconds()
        {
            double hours = 23.0 * 3600;
            double minutes = 56.0 * 60;
            double seconds = 4.0916;


            return (hours + minutes + seconds);
        }



        /// <summary>
        /// Calculates radius from latitude as determined by WGS84 data
        /// </summary>
  
        public static double radius(double latitude)
        {
            double eSquared = WGS84.eccentricitySquared;
            double radius = avgRadius * (1 - eSquared) / Math.Pow(1 - eSquared *
                Math.Sin(latitude) * Math.Sin(latitude), (3 / 2));

            return radius;
        }
    }
}
