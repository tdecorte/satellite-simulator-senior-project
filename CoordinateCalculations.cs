﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class CoordinateCalculations
    {

        private double m_rotatedX;
        private double m_rotatedY;
        private double m_rotatedZ;

        private double m_satInclination;
        
        private double m_angleFromPerigee;
        private double m_raan;

        private double m_rightAscension;
        private double m_precession;

        private Time m_time;

        private double m_secPerDay = 86400;
        private double m_degInCircle = 360.0;

        


        /// <summary>
        /// Calculates next coordinate of satellite's position assuming next position occurs after one second 
        /// </summary>

        public CoordinateCalculations(ref Satellite satellite, 
                                      ref KeplerEquations keplerEquations, 
                                      double index,
                                      Time time,
                                      double secsFromEphemeris)
        {
            // Increase time at a rate of which is set in Time class

            index *= Time.increaseRate;


            m_time = time;
            m_satInclination = satellite.almanacData.inclination;
            m_raan = satellite.almanacData.rateOfRA;
            m_rightAscension = satellite.almanacData.rightAscension;

            double latitude = satellite.coordinates.latitude;
            double longitude = satellite.coordinates.longitude;

            

            m_angleFromPerigee = keplerEquations.setTime(index);
            satellite.velocity = keplerEquations.instantVelocity;



            // Convert latitude, longitude coordinates to Catesian coordinates (ECEF)

            GeodeticConversions geoid = new GeodeticConversions();


            latitude = 0;
            longitude = 0;

            geoid.geodeticLLA2cartesian(latitude, longitude, satellite.altitude + Earth.radius(latitude));

            double x = geoid.x;
            double y = geoid.y;
            double z = geoid.z;



            // Calculate satellite orbital rotation coordinates    

            
            alignVernalEquinox(x, y, z, index, time, secsFromEphemeris);

            //alignVernalEquinox(x, y, z, index, time, veSecondsShift);

            satelliteRotationCoordinates(m_rotatedX, m_rotatedY, m_rotatedZ, index, time);

            //rotatedEarthCoordinates(m_rotatedX, m_rotatedY, m_rotatedZ, index);




            // Convert cartesian coordinates back to latitude, longitude, altitude 
            // for compatibility for Google Earth GUI                     

            geoid.cartesian2geodeticLLA(m_rotatedX, m_rotatedY, m_rotatedZ);

                        


            // Assign new values to satellite's coordinates

            satellite.altitude = keplerEquations.calcAltitude(geoid.latitude);
            satellite.coordinates.latitude = geoid.latitude;
            satellite.coordinates.longitude = geoid.longitude;


        }







        /// <summary>
        /// Uses rotational matrices to find the point after the rotation of Earth
        /// </summary>

        private void rotatedEarthCoordinates(double x, double y, double z, double index)
        {


            double angle = ((index * (Earth.rotationRate)) % m_degInCircle);
            
            angle = Maths.toRadians(angle);

            

            Matrix xyzMatrix = new Matrix(x, y, z);

            Matrix rotateZMatrix = new Matrix(Math.Cos(-angle), -Math.Sin(-angle), 0,
                                              Math.Sin(-angle), Math.Cos(-angle), 0,
                                              0, 0, 1);

            
            Matrix rotatedMatrix = new Matrix();

            rotatedMatrix = Matrix.multiply(rotateZMatrix, xyzMatrix);


            m_rotatedX = rotatedMatrix.getRow[0, 0];
            m_rotatedY = rotatedMatrix.getRow[1, 0];
            m_rotatedZ = rotatedMatrix.getRow[2, 0];
        }




        /// <summary>
        /// Calculates satellite's position change as x, y, z coordinates
        /// for satellite's orbital path.
        /// Note: Does not account for Earth's rotation
        //  Multiplies x, y, z coordinates by R_z matrix CCW by deg per second
        //  Multiplies result by R_x matrix CCW by inclination
        /// </summary>

        private void satelliteRotationCoordinates(double x, double y, double z, double index, Time time)
        {

            //double precession = (m_raan / Earth.sideRealSeconds()) *(index);// + time.ephemerisInSeconds);
            double precession = (m_raan * index);// % (2 * Math.PI);

            double setOrigin = time.alignOriginTime();
            
            
            m_angleFromPerigee = (m_angleFromPerigee + precession);// % (2 * Math.PI);
            double axialTilt = Maths.toRadians(Earth.axialTilt);


            Matrix xyzMatrix = new Matrix(x, y, z);

            Matrix initOriginMatrix = new Matrix((Math.Cos(setOrigin)), -(Math.Sin(setOrigin)), 0,
                                                 (Math.Sin(setOrigin)), (Math.Cos(setOrigin)), 0,
                                                 0, 0, 1);

            Matrix rotateZMatrix = new Matrix((Math.Cos(m_angleFromPerigee)), -(Math.Sin(m_angleFromPerigee)), 0,
                                              (Math.Sin(m_angleFromPerigee)), (Math.Cos(m_angleFromPerigee)), 0,
                                              0, 0, 1);

            Matrix rotateXMatrix = new Matrix(1, 0, 0,
                                              0, Math.Cos(m_satInclination), -Math.Sin(m_satInclination),
                                              0, Math.Sin(m_satInclination), Math.Cos(m_satInclination));

            Matrix shiftMatrix = new Matrix((Math.Cos(m_rightAscension)), (Math.Sin(m_rightAscension)), 0,
                                              -(Math.Sin(m_rightAscension)), (Math.Cos(m_rightAscension)), 0,
                                              0, 0, 1);

            Matrix rotateAxisMatrix = new Matrix(1, 0, 0,
                                              0, Math.Cos(axialTilt), -Math.Sin(axialTilt),
                                              0, Math.Sin(axialTilt), Math.Cos(axialTilt));

            Matrix rotatedMatrix = new Matrix();

            rotatedMatrix = Matrix.multiply(initOriginMatrix, xyzMatrix);
            rotatedMatrix = Matrix.multiply(rotateZMatrix, rotatedMatrix);
            
            rotatedMatrix = Matrix.multiply(rotateXMatrix, rotatedMatrix);
            rotatedMatrix = Matrix.multiply(shiftMatrix, rotatedMatrix);
            //rotatedMatrix = Matrix.multiply(rotateAxisMatrix, rotatedMatrix);

            m_rotatedX = (rotatedMatrix.getRow[0, 0]);
            m_rotatedY = (rotatedMatrix.getRow[1, 0]);
            m_rotatedZ = (rotatedMatrix.getRow[2, 0]);
        }




        private void alignVernalEquinox(double x, double y, double z, double index, Time time, double secsFromEphemeris)
        {

            // precession of earth rate / second

            double secsInMinute = 60;
            double minsInHour = 60;
            double hoursInDay = 24;
            double daysPerYear = 365.25;
            double secondsPerYear = secsInMinute * minsInHour * hoursInDay * daysPerYear;

            
            // precession of earth per second * time since ephemeris
            double radianShift = (Earth.rotationRate * secsFromEphemeris);

            //double radianShift = ((secsFromEphemeris * m_raan) + m_rightAscension);
            //double radianShift = ((m_raan) + (secsFromEphemeris * m_rightAscension));
            

            Matrix xyzMatrix = new Matrix(x, y, z);



            // Rotate about z-axis

            Matrix initOriginMatrix = new Matrix((Math.Cos(radianShift)), -(Math.Sin(radianShift)), 0,
                                                 (Math.Sin(radianShift)), (Math.Cos(radianShift)), 0,
                                                 0, 0, 1);

            Matrix rotatedMatrix = new Matrix();



            // Multiply original matrix by initial origin matrix

            rotatedMatrix = Matrix.multiply(initOriginMatrix, xyzMatrix);


            m_rotatedX = (rotatedMatrix.getRow[0, 0]);
            m_rotatedY = (rotatedMatrix.getRow[1, 0]);
            m_rotatedZ = (rotatedMatrix.getRow[2, 0]);
        }



        /*
        private void alignVernalEquinox(double x, double y, double z, double index, Time time, int veSecondsShift)
        {

            double veShift = (m_raan * veSecondsShift);

            Matrix xyzMatrix = new Matrix(x, y, z);

            Matrix initOriginMatrix = new Matrix((Math.Cos(veShift)), -(Math.Sin(veShift)), 0,
                                                 (Math.Sin(veShift)), (Math.Cos(veShift)), 0,
                                                 0, 0, 1);

            Matrix rotatedMatrix = new Matrix();

            rotatedMatrix = Matrix.multiply(initOriginMatrix, xyzMatrix);


            m_rotatedX = (rotatedMatrix.getRow[0, 0]);
            m_rotatedY = (rotatedMatrix.getRow[1, 0]);
            m_rotatedZ = (rotatedMatrix.getRow[2, 0]);
        }
         */

   

    }
}