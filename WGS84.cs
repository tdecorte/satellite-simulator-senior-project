﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    public static class WGS84
    {
        private const double m_equatorialRadius = 6378137.0;
        private const double m_flattening = (1 / 298.257223563);

        public static double flattening { get { return m_flattening; } }
        public static double equatorialRadius { get { return m_equatorialRadius; } }
        public static double semiMinorAxis { get { return (1 - flattening) * equatorialRadius; } }
        public static double eccentricitySquared { get { return (2 * flattening) - (flattening * flattening); } }
    }
}
