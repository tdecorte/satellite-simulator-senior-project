﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class Satellite
    {

        // Satellite variables:

        private string m_name;
        private double m_altitude;
        private double m_height;
        private double m_velocity;
        private double m_earthDistPerSec;
        private double m_spaceDistPerSec;
        private string m_orbitalType = "Medium Earth Orbit";
        private Coordinates m_coordinates;
        private AlmanacData m_almanacData;
    

        // Accessors

        public string name { get { return m_name; } set { m_name = value; } }
        public double height { get { return m_height; } set { m_height = value; } }
        public double velocity { get { return m_velocity; } set { m_velocity = value; } }        
        public double altitude { get { return m_altitude; } set { m_altitude = value; } }
        public string orbitalType { get { return m_orbitalType; } }
        public Coordinates coordinates { get { return m_coordinates; }  set { m_coordinates = value; } }
        public AlmanacData almanacData { get { return m_almanacData; } set { m_almanacData = value; } }



        // Constructor

        public Satellite()
        {
            m_almanacData = new AlmanacData();
            m_coordinates = new Coordinates();

            //m_height = Earth.avgRadius + m_altitude;
            
            //spaceDistPerSec();
            //earthDistPerSec();
        }


        
        /// <summary>
        /// Uses the oribital velocity formula to calculate the satellite's 
        /// velocity based on altitude
        /// </summary>
        
        private double velocityCalc()
        {
            double radius = m_height;
            return Math.Sqrt((Earth.gravitationalConst * Earth.mass) / radius);
        }


        
        /// <summary>
        /// Calculates the distance relative to Earth by finding the ratio 
        /// of circumferences of the satellite's orbit vs. circumference of Earth
        /// </summary>

        private void earthDistPerSec()
        {
            double circumference = 2 * Math.PI * Earth.avgRadius;

            double satelliteRadius = Earth.avgRadius + m_altitude;
            double spaceCircumference = 2 * Math.PI * satelliteRadius;
            double ratio = circumference / spaceCircumference;

            m_earthDistPerSec = velocityCalc() * ratio;
        }



        /// <summary>
        /// Distance per second is the same as velocity, as measured in m/s
        /// This calculation is the satellite's actual velocity while moving 
        /// through space
        /// </summary>

        private void spaceDistPerSec()
        {
            m_spaceDistPerSec = velocityCalc(); //26378100.0;
        }



        /// <summary>
        /// Returns the orbital degrees the satellite rotates each second.
        /// </summary>
      
        public double orbitDegPerSec()
        {
            double secPerHour = 3600;
            double orbitDegPerSec = 360 / (Earth.sideRealHours() / 2 * secPerHour);

            return orbitDegPerSec;
        }

    }
}
