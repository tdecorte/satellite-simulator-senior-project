﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SatelliteSimulator
{
    public partial class PleaseWaitForm : Form
    {
        Timer m_timer = new Timer();

        public PleaseWaitForm()
        {
            InitializeComponent();

            m_timer.Interval = 6000;
            m_timer.Tick += new EventHandler(timerTick);
            m_timer.Start();
        }

        void timerTick(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
