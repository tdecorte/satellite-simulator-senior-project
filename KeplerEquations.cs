﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatelliteSimulator
{
    class KeplerEquations
    {
        private double m_altitude;
        private double m_velocity;
        private double m_eccentricity;
        private double m_semiMajorAxis;

        private double m_meanMotion;
        private double m_meanAnomaly;
        private double m_meanAnomaly_0;
        private double m_eccentricAnomaly;
        private double m_trueAnomaly;

        private double m_instantVelocity;
        private double m_orbitalPeriod;

        private double m_time;
        private double m_epochTime;
        private double m_deltaN;



        // Set values needed for anomaly calculations specific to a satellite

        public KeplerEquations(double epochTime, 
                               double eccentricity, 
                               double semiMajorAxis, 
                               double meanAnomaly_0)
        {
            m_epochTime = epochTime;
            m_eccentricity = eccentricity;
            m_semiMajorAxis = semiMajorAxis;
            m_meanAnomaly_0 = meanAnomaly_0;
            
            calcMeanMotion();
        }



        // Accessors

        public double altitude { get { return m_altitude; } }
        public double velocity { get { return m_velocity; } }

        public double orbitalPeriod { get { return 2 * Math.PI * Math.Sqrt(Math.Pow(m_semiMajorAxis, 3) / 
                                        (Earth.gravitationalConst * Earth.mass)); } }

        public double instantVelocity { get { return m_instantVelocity; } }

                



        /// <summary>
        /// Calculates the degrees from perigee specific to time.
        /// </summary>

        public double setTime(double time)
        {
            m_time = (time);// +m_epochTime;
            
            calcMeanAnomaly();
            calcEccentricAnomaly();

            double trueAnomaly = calcTrueAnomaly();

            calcInstantVelocity();

            return trueAnomaly; 
        }

             
        
        
        
        // Mean motion calculated according to ICD-GPS-200CW standard (radians/second).
        // Calculates the average motion of the satellite.

        private void calcMeanMotion()
        {
            m_meanMotion = Math.Sqrt((Earth.gravitationalConst * Earth.mass) / (Math.Pow(m_semiMajorAxis, 3)));
            m_meanAnomaly_0 = m_meanAnomaly_0 - (m_meanMotion * m_epochTime);
        }





        // Mean anomaly calculated according to ICD-GPS-200CW standard. Time is the time measured
        // from epoch time. Mean motion and time are multiplied and added to mean anomaly at
        // epoch time to arrive at new mean anomaly, M(t).

        private void calcMeanAnomaly()
        {            
            m_meanAnomaly = m_meanAnomaly_0 + (m_meanMotion * m_time);

            if (m_meanAnomaly > Math.PI)
                m_meanAnomaly -= (2 * Math.PI);
        }

        



        // Eccentric anomaly uses Kepler's equation M_k = E_k - e * sin(E_k). 
        // This method uses the mean anomaly as the initial value for eccentric anomaly 
        // and iterates until the value converges at desired accuracy.

        private void calcEccentricAnomaly()
        {
            m_eccentricAnomaly = m_meanAnomaly;
           
            if (m_eccentricity > 0.8)
                m_eccentricAnomaly = Math.PI;

            for (int i = 0; i < 6; i++)
            {
                m_eccentricAnomaly = m_meanAnomaly + m_eccentricity * Math.Sin(m_eccentricAnomaly);
            }

        }





        // True anomaly is the calculation that locates the satellites actual position along its 
        // orbital path. The number of degrees from perigee is returned.

        private double calcTrueAnomaly()
        {
            double y = ((Math.Sqrt(1 - (m_eccentricity * m_eccentricity)) * 
                Math.Sin(m_eccentricAnomaly))) / 
                (1 - m_eccentricity * Math.Cos(m_eccentricAnomaly));

            double x = ((Math.Cos(m_eccentricAnomaly) - m_eccentricity) / 
                (1 - m_eccentricity * Math.Cos(m_eccentricAnomaly)));

            m_trueAnomaly = Math.Atan2(y,x);
            
            return m_trueAnomaly;
        }


       


        // Calculate instantaneous velocity

        private void calcInstantVelocity()
        {
            double radAlt = m_semiMajorAxis * 1 - m_eccentricity * Math.Cos(m_eccentricAnomaly);

            m_instantVelocity = Math.Sqrt((Earth.gravitationalConst * Earth.mass) * 
                                ((2 / (m_altitude + radAlt)) - (1 / m_semiMajorAxis)));
        }





        // Calculate altitude at current latitude and true anomaly

        public double calcAltitude(double latitude)
        {
            double radius = Earth.radius(latitude);
            return (m_semiMajorAxis * 1 - m_eccentricity * Math.Cos(m_eccentricAnomaly)) - radius;
        }

               
    }
}